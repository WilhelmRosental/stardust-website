import PostsContainer from "../PostsContainer/PostsContainer";

const Home = (props) => {
    return (
        <div className="container mt-4 mb-4">
            <PostsContainer/>
        </div>
    );
}

export default Home;