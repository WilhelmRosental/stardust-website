const Footer = (state) => {
    return(
      <footer className="footer mt-auto py-3 bg-light">
          <div className="container d-flex flex-row justify-content-center">
              <span className="text-muted text-footer">
                  © Groupomania 2021
              </span>
          </div>
      </footer>
    );
}

export default Footer;